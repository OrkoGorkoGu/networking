import requests, webbrowser
from bs4 import BeautifulSoup

# initialize session
session_requests = requests.session()

# url to the book's download page
book_url = "http://lib1.org/_ads/AAA44A42C8D43946076B82B88A619511"

# get info from webpage
response = session_requests.get(book_url)

# parse
soup = BeautifulSoup(response.text, "html.parser")

# get all links from the page
links = soup.find_all('a')

# loop through links and open the correct one (most will be ads)
for link in links:
    if "GET" in link:
        webbrowser.open(link['href'])
